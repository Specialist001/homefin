<?php

use yii\db\Migration;

/**
 * Class m190416_083805_add_income_expenses_table
 */
class m190416_083805_add_income_expenses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%income_expenses}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(11),
            'user_id' => $this->integer(11),
            'sum' => $this->integer(11),
            'type' => $this->string(100)->notNull(),
            'comments' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%income_expenses}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190416_083805_add_income_expenses_table cannot be reverted.\n";

        return false;
    }
    */
}
