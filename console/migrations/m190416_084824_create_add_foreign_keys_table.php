<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%add_foreign_keys}}`.
 */
class m190416_084824_create_add_foreign_keys_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createIndex(
            'idx-categories-parent_id',
            'categories',
            'parent_id'
        );

        $this->addForeignKey(
            'fk-categories-parent_id',
            'categories',
            'parent_id',
            'categories',
            'id'
        );

        $this->createIndex(
            'idx-income_expenses-category_id',
            'income_expenses',
            'category_id'
        );

        $this->addForeignKey(
            'fk-income_expenses-category_id',
            'income_expenses',
            'category_id',
            'categories',
            'id'
        );

        $this->createIndex(
            'idx-income_expenses-user_id',
            'income_expenses',
            'user_id'
        );

        $this->addForeignKey(
            'fk-income_expenses-user_id',
            'income_expenses',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-categories-parent_id',
            'categories'
        );

        $this->dropIndex(
            'idx-categories-parent_id',
            'categories'
        );

        $this->dropForeignKey(
            'fk-income_expenses-category_id',
            'income_expenses'
        );

        $this->dropIndex(
            'idx-income_expenses-category_id',
            'income_expenses'
        );

        $this->dropForeignKey(
            'fk-income_expenses-user_id',
            'income_expenses'
        );

        $this->dropIndex(
            'idx-income_expenses-user_id',
            'income_expenses'
        );
    }
}
