<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "budgets".
 *
 * @property int $id
 * @property int $sum
 * @property string $comments
 * @property int $created_at
 * @property int $updated_at
 */
class Budget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'budgets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sum', 'created_at', 'updated_at'], 'integer'],
            [['comments'], 'string'],
            [['created_at', 'updated_at'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Sum',
            'comments' => 'Comments',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
