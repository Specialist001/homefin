<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "income_expenses".
 *
 * @property int $id
 * @property int $category_id
 * @property int $user_id
 * @property int $sum
 * @property string $type
 * @property string $comments
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Category $category
 * @property User $user
 */
class IncomeExpense extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'income_expenses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'user_id', 'sum', 'created_at', 'updated_at'], 'integer'],
            [['type', 'created_at', 'updated_at'], 'required'],
            [['comments'], 'string'],
            [['type'], 'string', 'max' => 100],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
            'sum' => 'Sum',
            'type' => 'Type',
            'comments' => 'Comments',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
